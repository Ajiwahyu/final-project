﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace __RPL__RepositoryConsole_Client.Data
{
    class Item : Entity
    {
        public override string Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Price { get; set; }
    }
}
